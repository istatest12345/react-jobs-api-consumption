import axios from 'axios';
import React from 'react';
import { useQuery } from 'react-query';
import { useNavigate, useParams } from 'react-router';
import { Card, Grid, Text, Button, Row, Spacer, Badge, Modal, Input, Textarea } from "@nextui-org/react";

function SinglePost() {
	const { postId } = useParams();
	const navigate = useNavigate();
	const [visible, setVisible] = React.useState(false);
	const handler = () => setVisible(true);

	const closeHandler = () => {
		setVisible(false);
		console.log("closed");
	}

	const navigateTo = () => {
		navigate("/")
	}
	
	const updateHandler = async () => {
		let name = document.getElementById("name")
		let email = document.getElementById("email")
		let description = document.getElementById("description")
		let tags = document.getElementById("tags")
		let img = document.getElementById("img")

		let updatedData = {
			name: name.value,
			email: email.value,
			description: description.value,
			tags: tags.value,
			img: img.value
		}

		let res = await axios.put(`http://127.0.0.1:8000/api/jobs/${postId}`, updatedData)
		setVisible(false);
		navigate("/");

	}

	const getPost = async () => {
		const res = await axios.get(`http://127.0.0.1:8000/api/jobs/${postId}`)
		return res.data
	}

	const handleDelete = async () => {
		await axios.delete(`http://127.0.0.1:8000/api/jobs/${postId}`)
		navigate("/")
	}

	const { data, isLoading } = useQuery("post", getPost)

	return (
		<div>

			{isLoading ? "Loading..." : <Grid.Container gap={2} css={{ display: "flex", justifyContent: "center", alignItems: "center" }}>
				<Grid>
					<Card css={{ w: "50vw", h: "40vh" }}>
						<Card.Header>
							<img
								alt="nextui logo"
								src={data.img}
								width="70px"
								height="70px"
							/>
							<Grid.Container css={{ pl: "$6" }}>
								<Grid xs={12}>
									<Text h4 css={{ lineHeight: "$xs" }}>
										{data.name}
									</Text>
								</Grid>
								<Grid xs={12}>
									<Text css={{ color: "$accents8" }}>{data.email}</Text>
								</Grid>

							</Grid.Container>
						</Card.Header>
						{/* <Card.Divider /> */}
						<Card.Body css={{ py: "$10" }}>
							<Text>
								{data.description}
							</Text>
							<Spacer></Spacer>
							<Badge enableShadow disableOutline color="success">{data.tags}</Badge>
						</Card.Body>
						<Card.Divider />
						<Card.Footer>
							<Row justify="center">

								<Button onPress={navigateTo} size="sm" light>Home</Button>

								<Spacer />
								<Button size="sm" shadow color="warning" onClick={handler}>Edit</Button>
								<Spacer />

								<Button size="sm" shadow color="error" onPress={handleDelete}>Delete</Button>

							</Row>
						</Card.Footer>
					</Card>
				</Grid>
				<Modal
					blur
					closeButton
					aria-labelledby="modal-title"
					open={visible}
					onClose={closeHandler}
				>
					<Modal.Header>
						<Text id="modal-title" size={18}>
							Edit this post.
						</Text>
					</Modal.Header>
					<Modal.Body>
						<Input
							id='name'
							aria-label="name"
							clearable
							bordered
							fullWidth
							color="primary"
							size="lg"
							placeholder="Company Name"
							required

						/>
						<Input
							id='email'
							aria-label="email"
							clearable
							bordered
							fullWidth
							color="primary"
							size="lg"
							placeholder="Email"
							required

						/>
						<Textarea
							id='description'
							aria-label="description"
							clearable
							bordered
							fullWidth
							color="primary"
							size="lg"
							placeholder="Description"
							minRows={4}
							required
						/>
						<Input
							id='tags'
							aria-label="tags"
							clearable
							bordered
							fullWidth
							color="primary"
							size="lg"
							placeholder="Tags (coma seperated)"
							required

						/>
						<Input
							id='img'
							aria-label="img"
							clearable
							bordered
							fullWidth
							color="primary"
							size="lg"
							placeholder="Image URL"
							required

						/>

					</Modal.Body>
					<Modal.Footer>
						<Button auto color="error" onClick={closeHandler}>
							Close
						</Button>
						<Button auto onClick={updateHandler}>
							Update
						</Button>
					</Modal.Footer>
				</Modal>
			</Grid.Container>
			}
		</div>
	);
}

export default SinglePost;