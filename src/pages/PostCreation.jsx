import { Container, Text, Input, Textarea, Row, Spacer, Button } from '@nextui-org/react';
import axios from 'axios';
import React from 'react';

function PostCreation() {

	const handleSubmit = async (event) => {
		const myform = document.querySelector(".form1")
		const [name, email, description, tags, img] = myform.elements;
		const postedData = {
			name: name.value,
			email: email.value,
			description: description.value,
			tags: tags.value,
			img: img.value
		}
		event.preventDefault();
		await axios.post("http://127.0.0.1:8000/api/jobs", postedData);

	}
	return (
		<>
			<Spacer y={1} />
			<form className='form1' action="http://127.0.0.1:8000/api/jobs" method="post" onSubmit={handleSubmit} >
				<Spacer y={2} />
				<Container gap={5} style={{ width: "max-content" }}>
					<Row>
						<Text 
						css={{width: "400px", textAlign: "center"}}
						h3
						size={30}
						b
						> Create a job post</Text>
					</Row>
					<Spacer y={2} />
					<Row>
						<Input
							width='400px'
							bordered
							labelPlaceholder="Company name"
							color="primary" />
					</Row>
					<Spacer y={2} />
					<Row>
						<Input
							width='400px'
							bordered
							labelPlaceholder="Email"
							color="primary" />
					</Row>
					<Spacer y={2} />
					<Row>
						<Textarea
							width='400px'
							bordered
							labelPlaceholder="Description"
							minRows={5}
							color="primary" />
					</Row>
					<Spacer y={2} />
					<Row>
						<Input
							width='400px'
							bordered
							labelPlaceholder="Tags"
							color="primary" />
					</Row>
					<Spacer y={2} />
					<Row>
						<Input
							width='400px'
							bordered
							labelPlaceholder="Image URL"
							color="primary" />
					</Row>
					<Spacer y={2} />
					<Row>
						<Button
							color="success"
							shadow
							type='submit'
						>
							Create
						</Button>
					</Row>
					<Spacer y={2} />
				</Container >
				<Spacer y={2} />
			</form>
		</>
	);
}

export default PostCreation;