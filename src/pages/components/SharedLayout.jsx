import React from 'react';
import NavBar from './NavBar';
import Footer from './Footer';
import { Outlet } from 'react-router';
import SearchBar from './SearchBar';
import { Spacer } from '@nextui-org/react';

function SharedLayout() {
	return (
		<>
			<NavBar />
			<Spacer />
			<Spacer />
			<SearchBar />
			<Spacer />
			<Outlet />

			<Spacer y={2} />
			<Footer />
		</>
	);
}

export default SharedLayout;