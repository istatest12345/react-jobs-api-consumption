import { Container, Card, Row, Text, Input, Spacer, Button } from "@nextui-org/react";
import React from 'react';


export default function SearchBar() {
	return (

		<Container xl css={{ h: "30vh" }}>
			<Card className="myCard" css={{ h: "100%" }}>
				<Card.Body style={{ display: "flex", flexDirection: "column", justifyContent: "center" }}>
					<Row justify="center" align="center">
						<Text h1 size={45} color="white" css={{ m: 0 }}>
							Find your <Text  h1 size={"3rem"} css={{
								textGradient: "45deg, $purple600 -20%, $pink600 100%", display: "inline"
							}} >next</Text> Employer from <Text  h1 size={45} css={{
								textGradient: "45deg, $purple600 -20%, $pink600 100%", display: "inline"
							}} >home</Text>.
						</Text>
					</Row>
					<Spacer />
					<Row justify="center" align="center">
						<Input
							width="50%"
							bordered
							placeholder="Facebook, Google ..."
							color="default"
						/>
						<Spacer x={1} />
						<Button 
						rounded
						color="success"
						auto
						>Search</Button>
					</Row>
				</Card.Body>
			</Card>
		</Container>
	);
}