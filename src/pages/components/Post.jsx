import React from 'react';
import { Card, Text, Grid, Badge } from "@nextui-org/react";
import { Link } from 'react-router-dom';

function Post({ info: { id, img, name, email, tags } }) {

	return (
		<Grid xs={6} sm={3}>
			<Card css={{ p: "$6", mw: "400px" }}>
				<Card.Header>
					<img
						alt="img"
						src={img}
						width="50p"
						height="50p"
					/>
					<Grid.Container css={{ pl: "$6" }}>
						<Grid xs={12}>
							<Text h4 css={{ lineHeight: "$xs" }}>
								{name}
							</Text>
						</Grid>
						<Grid xs={12}>
							<Text css={{ color: "$accents8" }}>{email}</Text>
						</Grid>
					</Grid.Container>
				</Card.Header>
				<Card.Body css={{ py: "$2" }}>
					<Link style={{width: "fit-content", height: "55px", display: "flex", alignItems: "center"}} to="/">
					<Badge  enableShadow disableOutline color="secondary">{tags}</Badge>
					</Link>
				</Card.Body>
				<Card.Footer>
					<Link to={`posts/${id}`}>
						<Text size={15} b color='primary'>View company details</Text>
					</Link>
				</Card.Footer>
			</Card>
		</Grid>
	);
}

export default Post;