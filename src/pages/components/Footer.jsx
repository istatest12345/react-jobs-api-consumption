import { Container, Text } from '@nextui-org/react';
import React from 'react';


function Footer() {
	return (
		<Container fluid id='footer' justify='center' alignItems='center'>
			<Text size={15} color="default">
				Copyright © 2022 React app, Inc.
			</Text>
		</Container>
	);
}

export default Footer;