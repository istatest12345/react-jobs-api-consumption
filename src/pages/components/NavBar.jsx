import React from 'react';
import { Navbar, Button, Link, Text, Card, Radio } from "@nextui-org/react";
import { NavLink } from 'react-router-dom';

function NavBar() {
	return (
		<Navbar isBordered variant="sticky">
			<Navbar.Brand>
				<Text b color="inherit" hideIn="xs">
					ACME
				</Text>
			</Navbar.Brand>
			<Navbar.Content hideIn="xs">
				<NavLink  to="/">Home</NavLink>
				<NavLink  to="create">Create Post</NavLink>
				<NavLink to="contact">Contact</NavLink>
				<NavLink to="about">About</NavLink>
			</Navbar.Content>
			<Navbar.Content>
				<Navbar.Link color="inherit" href="#">
					Login
				</Navbar.Link>
				<Navbar.Item>
					<Button auto flat as={Link} href="#">
						Sign Up
					</Button>
				</Navbar.Item>
			</Navbar.Content>
		</Navbar>
		// <header>
		// 	<Link to="/">Home</Link>
		// 	<Link to="create">Post Creation</Link>
		// 	<Link to="contact">Contact</Link>
		// </header>
	);
}

export default NavBar;