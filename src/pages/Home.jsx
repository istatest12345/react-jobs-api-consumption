import React from 'react';
import axios from 'axios';
import { useQuery } from 'react-query';
import Post from './components/Post';
import { Container, Grid } from '@nextui-org/react';


function Home() {

	const getData = async () => {
		let response = await axios.get("http://127.0.0.1:8000/api/jobs");
		return response.data;
	}

	const { data, isLoading } = useQuery('posts', getData);

	return (
		<Container className='home' >
			<Grid.Container gap={2}  justify='flex-start'>
				{isLoading ? "Loading..." : data.map(item => <Post key={item.id} info={item} />)}
			</Grid.Container>
		</Container>
	);
}

export default Home;