import { Routes, Route, BrowserRouter } from 'react-router-dom';
import { QueryClient, QueryClientProvider } from 'react-query';
import { ReactQueryDevtools } from 'react-query/devtools';
import { NextUIProvider } from '@nextui-org/react';
import './App.css'
import Home from './pages/Home';
import PostCreation from './pages/PostCreation';
import SinglePost from './pages/SinglePost';
import SharedLayout from './pages/components/SharedLayout';
import { createTheme } from "@nextui-org/react"
import Search from './pages/Search';

// 2. Call `createTheme` and pass your custom values
const myDarkTheme = createTheme({
  type: 'dark'
})


const queryClient = new QueryClient();

function App() {


	return (
		<NextUIProvider theme={myDarkTheme}>
			<QueryClientProvider client={queryClient}>
				<BrowserRouter>
					<Routes>
						<Route path="/" element={<SharedLayout />} >
							<Route index element={<Home />} />
							<Route path="search?" element={<Search />} />
							<Route path="create" element={<PostCreation />} />
							<Route path="posts/:postId" element={<SinglePost />} />
							<Route path="contact" element={<h2>contact</h2>} />
							<Route path="*" element={<h2>404: page not found</h2>} />
						</Route>
					</Routes>
				</BrowserRouter>
				<ReactQueryDevtools />
			</QueryClientProvider>
		</NextUIProvider>
	);
}

export default App
